public class CalculateErrorException extends RuntimeException{

    public CalculateErrorException(String message) {
        super(message);
    }

    public CalculateErrorException() {
        super("Calculate Error");
    }
}
