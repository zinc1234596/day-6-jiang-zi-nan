import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class WordFrequencyGame {
    private static final String WORD_SPLIT_REGEX = "\\s+";

    public String countWordFrequency(String inputString) {
        try {
            List<String> splitWordList = splitInputString(inputString);
            Map<String, Integer> wordToFrequencyMap = createWordToFrequencyMap(splitWordList);
            List<Word> wordList = createInputListFromMap(wordToFrequencyMap);
            return createResultString(wordList);
        } catch (Exception e) {
            throw new CalculateErrorException();
        }
    }

    private List<String> splitInputString(String inputString) {
        return List.of(inputString.split(WORD_SPLIT_REGEX));
    }

    private Map<String, Integer> createWordToFrequencyMap(List<String> wordList) {
        return wordList.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.summingInt(frequency -> 1)));
    }

    private List<Word> createInputListFromMap(Map<String, Integer> wordToFrequencyMap) {
        return wordToFrequencyMap.entrySet().stream()
                .map(entry -> new Word(entry.getKey(), entry.getValue()))
                .sorted(Comparator.comparingInt(Word::getWordCount).reversed())
                .collect(Collectors.toList());
    }

    private String createResultString(List<Word> wordList) {
        return wordList.stream()
                .map(word -> word.getValue() + " " + word.getWordCount())
                .collect(Collectors.joining("\n"));
    }
}
