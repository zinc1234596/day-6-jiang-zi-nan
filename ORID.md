### O:

- Learned several Design Patterns including Strategy Pattern, Observer Pattern, and Command Pattern through group presentations. It is important to distinguish between Strategy and Command Patterns as they are very similar. Strategy Pattern is a design pattern for dynamically selecting algorithms or behaviors, while Command Pattern is a design pattern for encapsulating requests as objects for parameterization and passing.
- Refactoring is the process of improving the quality, readability, and maintainability of code by addressing code smells, which are potential issues or signs of poor design in the code. Refactoring follows four rules: test protection, find code smell, refactor techniques, and take baby steps.

### R:

- I need to digest the learning of Design Patterns slowly.

### I:

- Learning Design Patterns requires continuous practice and consideration based on business needs.
- Code refactoring is really helpful for reviewing requirements and coding.

### D:

Based on my personal exposure to some new or unfamiliar knowledge:

- WebSocket is a bidirectional communication protocol based on TCP protocol. It allows for the establishment of a persistent connection between a web browser and a server to exchange data in real time. WebSocket is commonly used in applications that require high real-time performance, such as instant messaging and online games. In front-end development, JavaScript's WebSocket API can be used to establish a connection with a server and send and receive data.
- The Open-Closed Principle states that software entities (such as classes, modules, functions, etc.) should be open for extension, but closed for modification.